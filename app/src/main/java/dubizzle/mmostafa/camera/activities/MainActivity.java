package dubizzle.mmostafa.camera.activities;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import dubizzle.mmostafa.camera.application.DuCameraApplication;
import dubizzle.mmostafa.camera.fragments.CameraFragment;
import dubizzle.mmostafa.camera.R;


public class MainActivity extends ActionBarActivity {

    // =============================================================================================
    // onCreate() ==================================================================================
    // =============================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                ex.printStackTrace();
                Log.d("TAG", ex.toString());
            }
        });

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        DuCameraApplication.setMainActivity(this);

        addCameraFragment();
    }

    // =============================================================================================
    // addCameraFragment() =========================================================================
    // =============================================================================================
    private void addCameraFragment() {

        final CameraFragment CAMERA_FRAGMENT = new CameraFragment();

        final FragmentTransaction FRAGMENT_TRANSACTION = getFragmentManager().beginTransaction();
        FRAGMENT_TRANSACTION.add(R.id.mainContainer, CAMERA_FRAGMENT, CameraFragment.CAMERA_TRAGMENT_TAG).commit();
    }
}
