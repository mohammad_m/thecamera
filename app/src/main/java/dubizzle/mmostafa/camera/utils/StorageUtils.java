package dubizzle.mmostafa.camera.utils;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mohammad on 5/30/15.
 */
public class StorageUtils {

    // =============================================================================================
    // savePhoto() =================================================================================
    // =============================================================================================
    public static void savePhoto(Bitmap bitmap){

        File pictureFile = getOutputMediaFile();
        if (pictureFile == null){

            return;
        }

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        catch (IOException e) {

            e.printStackTrace();
        }
    }

    // =============================================================================================
    // getOutputMediaFileUri() =====================================================================
    // =============================================================================================
    private static File getOutputMediaFile(){

        // This directory is shared (public), If your application is uninstalled,
        // media files saved to this location will not be removed.
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "DubizzleCamera");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){

                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }
}
