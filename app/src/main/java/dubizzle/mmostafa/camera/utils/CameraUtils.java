package dubizzle.mmostafa.camera.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.io.ByteArrayOutputStream;

import dubizzle.mmostafa.camera.R;
import dubizzle.mmostafa.camera.activities.MainActivity;
import dubizzle.mmostafa.camera.application.DuCameraApplication;

/**
 * Created by mohammad on 5/29/15.
 */
@SuppressWarnings("deprecation")
public class CameraUtils {

    public final static String PREF_CAMERA_ID = "current_camera_id";

    private static Dialog dialog;

    // =============================================================================================
    // getCameraInstance() =========================================================================
    // =============================================================================================
    public static Camera getCameraInstance(Context context, int cameraID) {

        Camera camera = null;

        if (hasBackCamera(context) || hasFrontCamera(context)) {

            try {

                camera = Camera.open(cameraID);
            } catch (Exception e) {

                // TODO show in use error msg ...
                Log.d("TAG", "couldn't open the camera: " + e.toString());
            }
        } else {

            // TODO show "camera not available" error msg ...
            Log.d("TAG", "Camera is not available");
        }

        return camera;
    }

    // =============================================================================================
    // isCameraAvailable() =========================================================================
    // =============================================================================================
    private static boolean hasBackCamera(Context context) {

        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

    }

    // =============================================================================================
    // isFrontCameraAvailable() ====================================================================
    // =============================================================================================
    public static boolean hasFrontCamera(Context context) {

        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
    }

    // =============================================================================================
    // rotateCamera() ==============================================================================
    // =============================================================================================
    public static void rotateAndAutoFocusCamera(Camera camera) {

        //STEP #1: Get rotation degrees
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(getCurrentCameraID(), info);

        int rotate = (info.orientation - getRotation() + 360) % 360;
        Log.d("TAG", "rotation: " + getRotation());
        //STEP #2: Set the 'rotation' parameter
        Camera.Parameters params = camera.getParameters();

        params.setRotation(rotate);

        if (params.getSupportedFocusModes() != null
                && params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {

            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        camera.setParameters(params);
    }

    // =============================================================================================
    // getRotation() ===============================================================================
    // =============================================================================================
    private static int getRotation() {

        return DuCameraApplication.getMainActivity().getWindowManager().getDefaultDisplay().getRotation();
    }

    // =============================================================================================
    // getRotation() ===============================================================================
    // =============================================================================================
    public static int getCurrentCameraID() {

        return DuCameraApplication.getSharedPreferences().getInt(PREF_CAMERA_ID, Camera.CameraInfo.CAMERA_FACING_BACK);
    }

    // =============================================================================================
    // showFlashOptionsDialog() ====================================================================
    // =============================================================================================
    public static void showFlashOptionsDialog(MainActivity mActivity, final Camera camera) {

        dialog = new Dialog(mActivity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_flash_options);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent_white);
        dialog.setCancelable(true);

        final Button onButton = (Button) dialog.findViewById(R.id.flashOnButton);
        onButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeFlashState(Camera.Parameters.FLASH_MODE_ON, camera);
            }
        });

        final Button offButton = (Button) dialog.findViewById(R.id.flashOffButton);
        offButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeFlashState(Camera.Parameters.FLASH_MODE_OFF, camera);
            }
        });

        final Button autoButton = (Button) dialog.findViewById(R.id.flashAutoButton);
        autoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeFlashState(Camera.Parameters.FLASH_MODE_AUTO, camera);
            }
        });

        dialog.show();
    }

    // =============================================================================================
    // changeFlashState() ==========================================================================
    // =============================================================================================
    private static void changeFlashState(String mode, Camera camera) {

        // TODO show the selected button the next time the popup is shown ...

        final Camera.Parameters PARAMS = camera.getParameters();

        if (!PARAMS.getFlashMode().equals(mode)) {

            PARAMS.setFlashMode(mode);
            camera.setParameters(PARAMS);
        }

        dialog.dismiss();
    }

    // =============================================================================================
    // setCameraDisplayOrientation() ===============================================================
    // =============================================================================================
    public static void setCameraDisplayOrientation(android.hardware.Camera camera) {

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(CameraUtils.getCurrentCameraID(), info);

        int result;

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + getRotation()) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - getRotation() + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    //==========================================================================================
    // getPic(int x, int y, int width, int height) =============================================
    //==========================================================================================
    public static Bitmap getPic(Camera camera, int x, int y, int width, int height) {

        System.gc();
        Bitmap b = null;
        Camera.Size s = camera.getParameters().getPreviewSize();

        YuvImage yuvimage = new YuvImage(CameraPreview.getBuffer(), ImageFormat.NV21, s.width, s.height, null);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(x, y, height, height), 100, outStream); // make JPG
        b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
        if (b != null) {
            //Log.i(TAG, "getPic() WxH:" + b.getWidth() + "x" + b.getHeight());
        } else {
            //Log.i(TAG, "getPic(): Bitmap is null..");
        }
        yuvimage = null;
        outStream = null;
        System.gc();
        return b;
    }

    //====================================================================================
    // getRatio() ========================================================================
    //====================================================================================
    // with this I get the ratio between screen size and pixels
    // of the image so I can capture only the rectangular area of the
    // image and save it.
    public static Double[] getRatio(Camera camera){

        DisplayMetrics displayMetrics = new DisplayMetrics();
        DuCameraApplication.getMainActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int mScreenHeight = displayMetrics.heightPixels;
        int mScreenWidth = displayMetrics.widthPixels;

        Camera.Size s = camera.getParameters().getPreviewSize();
        double heightRatio = (double)s.height/(double)mScreenHeight;
        double widthRatio = (double)s.width/(double)mScreenWidth;
        Double[] ratio = {heightRatio,widthRatio};
        return ratio;
    }

    // =============================================================================================
    // changeFooterHeight() ========================================================================
    // =============================================================================================
    public static void changeFooterHeight(RelativeLayout footer){

        Display display = DuCameraApplication.getMainActivity().getWindowManager().getDefaultDisplay();
        int screen_height = display.getHeight();
        int screen_width = display.getWidth();
        int footerHeight = screen_height - screen_width;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) footer.getLayoutParams();
        params.height = footerHeight;
        footer.setLayoutParams(params);
    }
}
