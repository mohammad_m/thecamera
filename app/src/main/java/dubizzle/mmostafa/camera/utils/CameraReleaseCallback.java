package dubizzle.mmostafa.camera.utils;

/**
 * Created by mohammad on 5/30/15.
 */
public interface CameraReleaseCallback {

    public void setCameraToNull();
}
