package dubizzle.mmostafa.camera.utils;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import dubizzle.mmostafa.camera.helpers.CameraHelper;

/**
 * Created by mohammad on 5/29/15.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, CameraReleaseCallback {

    private Camera camera;
    private SurfaceHolder mHolder;

    private static byte[] buffer;

    // =============================================================================================
    // ============================================================================================= Constructor
    // =============================================================================================
    public CameraPreview(Context context, Camera camera) {
        super(context);

        this.camera = camera;

        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    // =============================================================================================
    // surfaceCreated() ============================================================================
    // =============================================================================================
    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        // TODO specify the size ...

        CameraHelper.startCameraPreview(camera, holder);
    }

    // =============================================================================================
    // surfaceChanged() ============================================================================
    // =============================================================================================
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        CameraHelper.refreshCamera(camera, mHolder);
    }

    // =============================================================================================
    // surfaceDestroyed() ==========================================================================
    // =============================================================================================
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        CameraHelper.releaseCamera(camera, this, this);
    }

    // =============================================================================================
    // setCameraToNull() ===========================================================================
    // =============================================================================================
    @Override
    public void setCameraToNull() {

        camera = null;
    }

    // =============================================================================================
    // getBuffer() =================================================================================
    // =============================================================================================
    public static byte[] getBuffer(){

        return buffer;
    }

    // =============================================================================================
    // setBuffer() =================================================================================
    // =============================================================================================
    public static void setBuffer(byte[] mbuffer){

        buffer = mbuffer;
    }
}
