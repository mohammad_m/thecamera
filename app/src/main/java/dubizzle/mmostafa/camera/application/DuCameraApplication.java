package dubizzle.mmostafa.camera.application;

import android.app.Application;
import android.content.SharedPreferences;

import dubizzle.mmostafa.camera.activities.MainActivity;

/**
 * Created by mohammad on 5/30/15.
 */
public class DuCameraApplication extends Application {

    private static MainActivity mainActivity;

    public static void setMainActivity(MainActivity activity){

        mainActivity = activity;
    }

    public static MainActivity getMainActivity(){
        return mainActivity;
    }

    public static SharedPreferences getSharedPreferences(){

        return getMainActivity().getPreferences(MODE_PRIVATE);
    }
}
