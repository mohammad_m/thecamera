package dubizzle.mmostafa.camera.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import dubizzle.mmostafa.camera.R;
import dubizzle.mmostafa.camera.activities.MainActivity;
import dubizzle.mmostafa.camera.application.DuCameraApplication;
import dubizzle.mmostafa.camera.helpers.CameraHelper;
import dubizzle.mmostafa.camera.utils.CameraPreview;
import dubizzle.mmostafa.camera.utils.CameraReleaseCallback;
import dubizzle.mmostafa.camera.utils.CameraUtils;

/**
 * Created by mohammad on 5/29/15.
 */
@SuppressWarnings("deprecation")
public class CameraFragment extends Fragment implements View.OnClickListener,
        CameraReleaseCallback{

    public static final String CAMERA_TRAGMENT_TAG = "camera_fragment";

    private View view;
    private MainActivity mainActivity;

    private Camera camera;
    private CameraPreview cameraPreview;

    // =============================================================================================
    // onCreateView() ==============================================================================
    // =============================================================================================
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.camera_fragment, container, false);
        return view;
    }

    // =============================================================================================
    // onActivityCreated() =========================================================================
    // =============================================================================================
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mainActivity = (MainActivity) getActivity();

        setupView();
    }

    // =============================================================================================
    // onStart() ===================================================================================
    // =============================================================================================
    @Override
    public void onResume() {
        super.onResume();

        setupCamera();
    }

    // =============================================================================================
    // setupView() =================================================================================
    // =============================================================================================
    private void setupView() {

        final Button captureButton = (Button) view.findViewById(R.id.button_capture);
        captureButton.setOnClickListener(this);

        final Button FLASH_BUTTON = (Button) view.findViewById(R.id.flashButton);
        FLASH_BUTTON.setOnClickListener(this);

        // if the fornt camera is not available, don't show the button ...
        if (CameraUtils.hasFrontCamera(mainActivity.getApplicationContext())) {

            final Button SWITCH_BUTTON = (Button) view.findViewById(R.id.switch_button);
            SWITCH_BUTTON.setVisibility(View.VISIBLE);
            SWITCH_BUTTON.setOnClickListener(this);
        }

        // Change the height of the footer to overlay the camera, to get the square illusion ...
        final RelativeLayout FOOTER = (RelativeLayout) view.findViewById(R.id.footer);
        CameraUtils.changeFooterHeight(FOOTER);
    }

    // =============================================================================================
    // setupCamera() ===============================================================================
    // =============================================================================================
    private void setupCamera() {

        SharedPreferences preferences = mainActivity.getPreferences(Context.MODE_PRIVATE);

        camera = CameraUtils.getCameraInstance(mainActivity.getApplicationContext(),
                preferences.getInt(CameraUtils.PREF_CAMERA_ID, Camera.CameraInfo.CAMERA_FACING_BACK));

        if (camera != null) {

            cameraPreview = new CameraPreview(mainActivity.getApplicationContext(), camera);

            FrameLayout previewLayout = (FrameLayout) view.findViewById(R.id.camera_preview);

            previewLayout.removeAllViews();
            previewLayout.addView(cameraPreview);
        }

        // disable/enable the flash option ...
        CameraHelper.disableEnableFlashButton(CameraUtils.getCurrentCameraID(), (Button) view.findViewById(R.id.flashButton));
    }

    // =============================================================================================
    // onPause() ===================================================================================
    // =============================================================================================
    @Override
    public void onPause() {
        super.onPause();

        CameraHelper.releaseCamera(camera, cameraPreview, this);
    }

    // =============================================================================================
    // onClick() ===================================================================================
    // =============================================================================================
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.button_capture:

                CameraHelper.savePhotoAndRefresh(camera, cameraPreview.getHolder());

                break;

            case R.id.switch_button:

                switchCamera();

                break;

            case R.id.flashButton:

                CameraUtils.showFlashOptionsDialog(mainActivity, camera);

                break;

            default:
                break;
        }
    }

    // =============================================================================================
    // savePhotoAndRefresh() =======================================================================
    // =============================================================================================
    private void switchCamera() {

        final int cameraID = CameraUtils.getCurrentCameraID() == Camera.CameraInfo.CAMERA_FACING_BACK ?
                Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK;


        DuCameraApplication.getSharedPreferences().edit().putInt(CameraUtils.PREF_CAMERA_ID, cameraID).apply();

        CameraHelper.releaseCamera(camera, cameraPreview, this);

        setupCamera();
    }

    // =============================================================================================
    // setCameraToNull() ===========================================================================
    // =============================================================================================
    @Override
    public void setCameraToNull() {

        camera = null;
    }
}
