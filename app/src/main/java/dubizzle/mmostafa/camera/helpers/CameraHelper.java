package dubizzle.mmostafa.camera.helpers;

import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.widget.Button;

import java.io.IOException;

import dubizzle.mmostafa.camera.application.DuCameraApplication;
import dubizzle.mmostafa.camera.utils.CameraPreview;
import dubizzle.mmostafa.camera.utils.CameraReleaseCallback;
import dubizzle.mmostafa.camera.utils.CameraUtils;
import dubizzle.mmostafa.camera.utils.StorageUtils;

/**
 * Created by mohammad on 5/30/15.
 */
@SuppressWarnings("deprecation")
public class CameraHelper {

    // =============================================================================================
    // startCameraPreview() ========================================================================
    // =============================================================================================
    public static void startCameraPreview(final Camera camera, SurfaceHolder holder) {

        try {
            camera.setPreviewDisplay(holder);
            // to fix the orientation on portrait ...
            CameraUtils.setCameraDisplayOrientation(camera);
            updateBufferSize(camera);
            camera.addCallbackBuffer(CameraPreview.getBuffer());
            camera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
                public synchronized void onPreviewFrame(byte[] data, Camera c) {

                    if (camera != null) { // there was a race condition when onStop() was called..
                        camera.addCallbackBuffer(CameraPreview.getBuffer()); // it was consumed by the call, add it back
                    }
                }
            });
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // =============================================================================================
    // refreshCamera() =============================================================================
    // =============================================================================================
    public static void refreshCamera(Camera camera, SurfaceHolder holder) {

        if (holder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        try {
            camera.stopPreview();
        } catch (Exception e) {

            e.printStackTrace();
        }

        // rotate ...
        CameraUtils.rotateAndAutoFocusCamera(camera);

        try {

            startCameraPreview(camera, holder);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    // =============================================================================================
    // savePhotoAndRefresh() =======================================================================
    // =============================================================================================
    public static void savePhotoAndRefresh(final Camera camera, final SurfaceHolder holder) {

        final Handler HANDLER = new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {

                Display display = DuCameraApplication.getMainActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                int height = size.y;

                Double[] ratio = CameraUtils.getRatio(camera);
                int left = (int) (ratio[1]*(double) 0);
                int top = (int) (ratio[0]*(double) 0);
                int right = (int)(ratio[1]*(double) width);
                int bottom = (int)(ratio[0]*(double) height);

                Bitmap bitmap = CameraUtils.getPic(camera, left, top, right, bottom);

                // the is saved rotated by 90 degrees, so it should be fixed before saving it ...
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                // I/O operations block the UI thread, so that it should run in a background thread...
                StorageUtils.savePhoto(bitmap);

                HANDLER.post(new Runnable() {
                    @Override
                    public void run() {

                        // refresh the view to take more photos.. prevent the camera from being stopped...
                        refreshCamera(camera, holder);
                    }
                });
            }
        }).start();
    }

    // =============================================================================================
    // releaseCamera() =============================================================================
    // =============================================================================================
    public static void releaseCamera(Camera camera, CameraPreview cameraPreview, CameraReleaseCallback callback) {

        try {
            if (camera != null) {

                camera.setPreviewCallback(null);
                cameraPreview.getHolder().removeCallback(cameraPreview);
                camera.stopPreview();
                camera.release();

                callback.setCameraToNull();
            }
        } catch (Exception e) {

            Log.d("TAG", "Exception while releasing" + e.toString());
        }
    }

    // =============================================================================================
    // releaseCamera() =============================================================================
    // =============================================================================================
    public static void disableEnableFlashButton(int cameraID, Button FlashButton) {


        if (cameraID == Camera.CameraInfo.CAMERA_FACING_FRONT) {

            FlashButton.setEnabled(false);
        } else {

            FlashButton.setEnabled(true);
        }
    }

    //==========================================================================================
    // updateBufferSize() ======================================================================
    //==========================================================================================
    private static void updateBufferSize(Camera camera) {
        CameraPreview.setBuffer(null);
        System.gc();
        // prepare a buffer for copying preview data to
        int h = camera.getParameters().getPreviewSize().height;
        int w = camera.getParameters().getPreviewSize().width;
        int bitsPerPixel = ImageFormat.getBitsPerPixel(camera.getParameters().getPreviewFormat());
        CameraPreview.setBuffer(new byte[w * h * bitsPerPixel / 8]);
    }
}
